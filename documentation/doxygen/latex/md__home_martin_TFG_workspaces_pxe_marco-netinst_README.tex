The minimal Arch Linux netinstaller for Raspberry Pi Models 1\-B, 1\-B+, 2\-B

This project, based on \href{https://github.com/debian-pi/raspbian-ua-netinst}{\tt raspbian-\/ua-\/netinst}, allows users of Arch Linux to install the base system (using Arch Linux A\-R\-M official tarballs or a custom-\/defined tarball, using Marco\-Polo). As such, it is a great tool for sysadmins, due to the fact that the installation can be left completely unattended, and only $\sim$15 M\-B have to be manually copied to an S\-D card.

The default settings uses D\-H\-C\-P for network configuration, wipes and formats the S\-D card, gets all the necessary files and copies them. Further configuration can be performed using standard bash scripts.

\subsection*{Features}


\begin{DoxyItemize}
\item completely unattended, you only need working Internet connection through the Ethernet port
\item D\-H\-C\-P and static ip configuration (D\-H\-C\-P is the default)
\item always installs the latest version of Arch Linux, allowing the usage of a custom version
\item configurable default settings
\item extra configuration over H\-T\-T\-P possible -\/ gives unlimited flexibility
\item installation takes about {\bfseries 15 minutes} with fast Internet from power on to sshd running
\item /tmp is mounted as tmpfs to improve speed
\item no clutter included, you only get the bare essential packages
\item option to install root to U\-S\-B drive
\end{DoxyItemize}

\subsection*{Requirements}


\begin{DoxyItemize}
\item a Raspberry Pi Model 1\-B, 1\-B+ or 2\-B
\item S\-D card of at least 640\-M\-B
\item working Ethernet with Internet connectivity
\end{DoxyItemize}

\subsection*{Obtaining installer files on Windows and Mac}

Installer archive is around {\bfseries 17\-M\-B} and contains all firmware files and the installer.

\subsection*{Deploying on Linux}

The script {\itshape \hyperlink{deploy_8sh}{deploy.\-sh}} should take care of everything in Linux environments when the dependency packages are properly installed.

\subsection*{Installing}

In normal circumstances, you can just power on your Pi and cross your fingers.

If you don't have a display attached you can monitor the Ethernet card leds to guess activity. When it finally reboots after installing everything you will see them going out and on a few times when Raspbian configures it on boot.

If you do have a display, you can follow the progress and catch any possible errors in the default configuration or your own modifications. If you have a serial cable, then remove 'console=tty1' at then end of the {\ttfamily cmdline.\-txt} file.

{\bfseries Note\-:} During the installation you'll see various warning messages, like \char`\"{}\-Warning\-: cannot read table of mounted file systems\char`\"{} and \char`\"{}dpkg\-: warning\-: ignoring pre-\/dependency problem!\char`\"{}. Those are expected and harmless.

\subsubsection*{Logging}

The output of the installation process is now also logged to file. When the installation completes successfully, the logfile is moved to /var/log/archlinux-\/netinst.log on the installed system. When an error occurs during install, the logfile is moved to the sd card, which gets normally mounted on /boot/ and will be named archlinux-\/netinst-\/$<$datetimestamp$>$.log

\subsection*{First boot}

The system is almost completely unconfigured on first boot. Here are some tasks you most definitely want to do on first boot.

The default {\bfseries root} password is {\bfseries root}.

\subsection*{Installer customization}

While defaults should work for most power users, some might want to customize default configuration or the package set even further. The installer provides support for this by reading a configuration file {\ttfamily installer-\/config.\-txt} from the first vfat partition. The configuration file is read in as a shell script so you can abuse that fact if you so want to. See {\ttfamily scripts/etc/init.\-d/rc\-S} for more details of what kind of environment your script will be run in (currently 'busybox sh').

If an {\ttfamily installer-\/config.\-txt} file exists in the same directory as this {\ttfamily R\-E\-A\-D\-M\-E.\-md}, it will be added to the installer image automatically.

The format of the file and the current defaults\-: \begin{DoxyVerb}preset=server
packages= # comma separated list of extra packages
mirror=http://mirrordirector.raspbian.org/raspbian/
release=wheezy
hostname=pi
domainname=
rootpw=raspbian
cdebootstrap_cmdline=
bootsize=+128M # /boot partition size in megabytes, provide it in the form '+<number>M' (without quotes)
rootsize=     # / partition size in megabytes, provide it in the form '+<number>M' (without quotes), leave empty to use all free space
timeserver=time.nist.gov
ip_addr=dhcp
ip_netmask=0.0.0.0
ip_broadcast=0.0.0.0
ip_gateway=0.0.0.0
ip_nameservers=
online_config= # URL to extra config that will be executed after installer-config.txt
usbroot= # set to 1 to install to first USB disk
cmdline="dwc_otg.lpm_enable=0 console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty1 elevator=deadline"
rootfstype=ext4
rootfs_mkfs_options=
rootfs_install_mount_options='noatime,data=writeback,nobarrier,noinit_itable'
rootfs_mount_options='errors=remount-ro,noatime'
\end{DoxyVerb}


All of the configuration options should be clear. You can override any of these in your {\itshape installer-\/config.\-txt} by placing your own {\ttfamily installer-\/config.\-txt} in the main directory. The time server is only used during installation and is for {\itshape rdate} which doesn't support the N\-T\-P protocol. {\bfseries Note\-:} You only need to provide the options which you want to {\bfseries override} in your {\itshape installer-\/config.\-txt} file. All non-\/provided options will use the defaults as mentioned above.

There's also support for a {\ttfamily post-\/install.\-txt} script which is executed just before unmounting the filesystems. You can use it to tweak and finalize your automatic installation. Just like above, if {\ttfamily post-\/install.\-txt} exists in the same directory as this {\ttfamily R\-E\-A\-D\-M\-E.\-md}, it will be added to the installer image automatically.

\subsection*{Disclaimer}

I take no responsibility for A\-N\-Y data loss. You will be reflashing your S\-D card anyway so it should be very clear to you what you are doing and will lose all your data on the card. Same goes for reinstallation.

See L\-I\-C\-E\-N\-S\-E for license information. 