%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Structured General Purpose Assignment
% LaTeX Template
%
% This template has been downloaded from:
% http://www.latextemplates.com
%
% Original author:
% Ted Pavlic (http://www.tedpavlic.com)
%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%----------------------------------------------------------------------------------------
%   PACKAGES AND OTHER DOCUMENT CONFIGURATIONS
%----------------------------------------------------------------------------------------

\documentclass{article}

\usepackage{fancyhdr} % Required for custom headers
\usepackage{lastpage} % Required to determine the last page for the footer
\usepackage{extramarks} % Required for headers and footers
\usepackage{graphicx} % Required to insert images
\usepackage{lipsum} % Used for inserting dummy 'Lorem ipsum' text into the template

\usepackage[T1]{fontenc} % Codificación de las fuentes utilizadas
\usepackage[spanish]{babel} % Español como idioma principal del texto (permite hyphenation de palabras al final de una línea)
\selectlanguage{spanish}
\usepackage{hyperref}
\hypersetup{
	colorlinks=true,
	urlcolor=blue,
	linkcolor=blue,
	citecolor=blue
}
\usepackage{listings}

\input{includes}
\usepackage{float}
%----------------------------------------------------------------------------------------
%   NAME AND CLASS SECTION
%----------------------------------------------------------------------------------------

\newcommand{\hmwkTitle}{Marcobootstrap} % Assignment title
\newcommand{\hmwkDueDate}{Lunes,\ 11\ de\ mayo\ de\ 2015} % Due date
\newcommand{\hmwkAuthorName}{Diego Martín Arroyo} % Your name

\graphicspath{{Figures/}}
%----------------------------------------------------------------------------------------
%   TITLE PAGE
%----------------------------------------------------------------------------------------

\title{\hmwkTitle}
\author{\textbf{\hmwkAuthorName}}
\date{\hmwkDueDate}

%----------------------------------------------------------------------------------------



\begin{document}

\maketitle
\begin{abstract}
El paquete Marcobootstrap comprende un conjunto de utilidades que permite llevar a cabo la descarga, instalación y actualización de un sistema operativo en varios nodos sin requerir la supervisión de un administrador, incluyendo el descubrimiento de los equipos necesarios para el desarrollo de dichas operaciones. Junto a estas se incluye una serie de aplicaciones de gestión del sistema utilizables por el administrador del sistema.
\end{abstract}

%----------------------------------------------------------------------------------------
%   TABLE OF CONTENTS
%----------------------------------------------------------------------------------------

%\setcounter{tocdepth}{1}

\tableofcontents
\newpage

\section{Introducción}

El uso de un sistema conformado por una cantidad significativa de nodos dificulta su mantenimiento, y en particular la instalación de un conjunto de herramientas iniciales y las posteriores actualizaciones de las mismas. Dicho mantenimiento, si es llevado a cabo de forma ``manual'' suele propiciar fallos humanos o inconsistencias en el resultado final (nodos que no cuentan con el mismo conjunto de herramientas o con configuraciones diferentes). Por ello, en el desarrollo del sistema se ha apostado por una herramienta que automatice la instalación del sistema operativo reduciendo al máximo el tiempo de atención que el administrador debe prestar a cada nodo, facilitando además la gestión y actualización posterior. Dichas herramientas se apoyan sobre los principios básicos de desarrollo seguidos en todo el sistema, entre los que destaca el descubrimiento automático de equipos.

En el presente documento se detallan las decisiones de diseño llevadas a cabo en el desarrollo de las utilidades \textbf{marco-netinst}, \textbf{marco-bootstrap}, y \textbf{marco-bootstrap-backend} que llevan a cabo este cometido, así como el funcionamiento de las mismas.

\section{Búsqueda de solución}

\textbf{marco-netinst} es la herramienta principal del sistema, que permite realizar instalaciones y actualizaciones de un sistema operativo inspirándose en el funcionamiento de un servidor \textbf{PXE}, requiriendo únicamente un conjunto reducido de herramientas preinstaladas en la tarjeta SD para realizar todas las tareas. Se integra con \textbf{MarcoPolo}, por lo que no es necesario realizar ningún tipo de configuración para realizar el proceso de instalación previamente.

La complejidad del problema a resolver es significativa, pues implica la creación de una herramienta que debe funcionar en red sin un sistema operativo completo, en un entorno en el cual la funcionalidad disponible es mínima.

\subsection{Definición de requisitos}

\begin{itemize}
\item El software no debe requerir configuración por parte del administrador, debe ser capaz de detectar toda la información necesaria gracias a las herramientas provistas.
\item El tiempo de interacción con la herramienta debe ser nulo o mínimo.
\item El nodo debe ser capaz de integrarse en el sistema sin ningún tipo de configuración posterior a la instalación.
\item Las herramientas creadas deben ser flexibles, a fin de poder adaptarlas a nuevas situaciones.
\end{itemize}

\subsection{Evaluación de alternativas}

Como se define en la memoria del Trabajo, las opciones típicamente utilizadas por administradores de sistemas no son viables en el sistema a construir debido a las características de la plataforma utilizada: los equipos \textbf{Rasbperry Pi} no soportan el protocolo \textbf{PXE}, debido a que el mismo no se incluye en el \textit{software} que provee la tarjeta de red (como suele ocurrir en equipos de escritorio, en los que se integra como módulo de la \textbf{BIOS}), por lo que es necesario crear una herramienta propia, o buscar alternativas ya existentes creadas para solucionar un problema similar.

\subsubsection{Estrategia inicial}

El arranque de una placa \textbf{Rasbperry Pi} es llevado a cabo principalmente por la GPU de la misma. Al conectarse a la corriente eléctrica, se activa la secuencia de arranque definida en la memoria de solo lectura. Este código busca en la primera partición de la tarjeta SD un \textit{bootloader} y lo carga, activando la memoria SDRAM y pasando al siguiente estado, en el que se carga el archivo start.elf, que carga el Kernel del sistema operativo según los parámetros definidos en los ficheros \texttt{config.txt} (pues no tiene un cargador que indique los parámetros, como ocurre en sistemas de escritorio, en los que existen opciones como GRUB o LILO), \texttt{cmdline.txt} y \textbf{bcm2835.dtb}.

\begin{figure}[H]
	\centering
	\includegraphics[width=0.4\textwidth]{bootsequence}
	\caption{Secuencia de arranque de la Raspberry Pi\cite{bootsequence}}
	\label{bootsequence}
\end{figure}

Un archivo de configuración \texttt{cmdline.txt} sin modificar suele seguir la siguiente estructura:

\begin{lstlisting}[frame=single,basicstyle=\small\ttfamily]
root=/dev/mmcblk0p2 rw rootwait console=ttyAMA0,115200 console=tty1 \
selinux=0 plymouth.enable=0 smsc95xx.turbo_mode=N dwc_otg.lpm_enable=0 \
kgdboc=ttyAMA0,115200 elevator=noop
\end{lstlisting}

Los parámetros de interés son los siguientes:

\begin{itemize}
	\item \texttt{root}: Partición raíz a utilizar (generalmente la segunda partición)
	\item \texttt{init}: Especifica el archivo a ejecutar en el arranque. Este proceso tendrá por tanto el \textbf{PID} 1 y generalmente su ejecutable es el fichero \texttt{/sbin/init}\footnote{En sistemas donde \texttt{systemd} es el proceso inicial, \texttt{/sbin/init} es un enlace simbólico al ejecutable de \texttt{systemd}}. Si no se especifica, como ocurre en el archivo por defecto, su valor es este\cite{kernelparams:init}.
\end{itemize}

Modificando el parámetro \texttt{init} es posible ejecutar un código diferente a \texttt{init} en el proceso de arranque, que realice la descarga e instalación del sistema operativo, logrando una solución de compromiso entre la instalación manual y el uso de \textbf{PXE}.

Debido a la falta de experiencia en el desarrollo de herramientas similares, se han seguido varios enfoques hasta dar con la solución adecuada:

\subsubsection{Primer enfoque: modificación del script de inicio}

En este primer intento se crea un \textit{script} y un ejecutable programado en C con un conjunto mínimo de utilidades que permitan la descarga del sistema operativo (utilizando herramientas como \textbf{cURL}. Estos archivos son instalados en la primera partición de la tarjeta SD, siendo correctamente ejecutados. Sin embargo, no es posible llevar a cabo la instalación debido a la falta de herramientas de configuración de red, y el enfoque presenta varios problemas adicionales: al carecer de un proceso \texttt{init}, el sistema es muy inestable, y es difícil realizar cualquier tarea más allá de la gestión de ficheros o tareas de mantenimiento básicas.

\subsubsection{Segundo enfoque: Busybox}

\textbf{Busybox} es una utilidad que aglutina un conjunto de herramientas UNIX en un único ejecutable de tamaño muy reducido, reemplazando así el uso de varios paquetes en unidades independientes, como \textbf{fileutils} o \textbf{shellutils}. Las utilidades a las que reemplaza cuentan con mucha menos funcionalidad, pero son capaces de crear un sistema utilizable con únicamente un \textit{kernel} y varios ficheros de configuración\cite{aboutbusybox}. Es la opción más común a la hora de crear sistemas embebidos o para realizar operaciones sin un sistema operativo completo.

Utilizando BusyBox es posible configurar la interfaz de red de la Raspberry Pi mediante \textbf{DHCP} gracias a la herramienta \textbf{udhcpc}, incluida en la distribución de BusyBox estándar, así como a un conjunto de herramientas de utilidad como \textbf{wget}.\\

Incluyendo BusyBox en el código del primer proyecto es posible conseguir acceso a la red, pero el hecho de no utilizar el proceso \textbf{init} dificulta la mayor parte de operaciones, por lo que esta opción es descartada.

\subsubsection{Enfoque exitoso}

Antes de detallar la solución definitiva al problema, es necesario detallar las herramientas de terceros valoradas y descartadas:

\paragraph{Raspi-LTSP, PiNET\\}

Estos proyectos\cite{raspiltsp, pinet} están enfocados a la centralización de toda la información en un servidor, incluyendo la carga del sistema operativo, por lo que no constituyen una opción viable.

\paragraph{BerryBoot\\}

BerryBoot\cite{berryboot} es un sistema de arranque que posibilita la descarga e instalación de un sistema operativo y la coexistencia de varios en una misma tarjeta SD. Constituye una opción bastante prometedora, que sin embargo depende significativamente de una interfaz gráfica y de un usuario que interaccione con la misma, haciendo que su uso sea inviable para el objetivo deseado sin una intensa reestructuración.

\subsection{Propuesta de solución}

El proyecto \textbf{raspbian-ua-netinst}\cite{raspbian-ua-netinst} posibilita la instalación de la distribución Raspbian desde un repositorio público, realizando el particionado de la tarjeta SD, la descarga e instalación de los ficheros y el resto de tareas necesarias desde la propia máquina, únicamente copiando en la tarjeta unos 15 \textit{megabytes} de información. Este proyecto ha sido adaptado a las necesidades específicas del sistema, con las siguientes modificaciones:

\begin{itemize}
	\item Instalación de \textbf{ArchLinux ARM} en lugar de \textbf{Raspbian}.
	\item Instalación del sistema operativo completo a partir de un archivo \textbf{.tar.gz} en lugar de la descarga de paquetes.
	\item Nuevo \textit{script} de carga del \textit{software} en la tarjeta SD (en el paquete original se delega a utilidades de terceros).
	\item Detección del servidor sin configuración previa utilizando \textbf{MarcoPolo}.
	\item Descarga mediante el protocolo HTTPS con validación doble de certificado.
	\item Se añaden funcionalidades de actualización.
	\item Interfaz administrativa.
\end{itemize}

El sistema se compone de varios componentes, como se detalla en la siguiente sección.

\section{marco-netinst}

\textbf{marco-netinst} está compuesto por un conjunto de herramientas que permiten la gestión de los nodos del sistema y la instalación del sistema operativo en los mismos. Para ello se compone de varios módulos:

\subsection{marco-netinst}

Este módulo, basado en \textbf{raspbian-ua-netinst} únicamente comprende varios \textit{scripts} y ficheros de configuración, detallados a continuación:

\subsubsection*{update.sh}

Descarga del repositorio indicado los paquetes que necesita el sistema operativo para arrancar. Dichos paquetes consisten básicamente en código de arranque, \textit{kernels}, bibliotecas y un conjunto de herramientas básicas, siendo la más importante de ellas \textbf{BusyBox}. El \textit{script} realiza la descarga en un directorio temporal de los archivos \textbf{.deb}.

\subsubsection*{build.sh}

Crea los archivos necesarios para la instalación, que deberán ser copiados a la tarjeta SD. Para ello, realiza la siguiente secuencia de acción:

\begin{enumerate}
\item Copia del \textit{kernel}.
\item Creación de los archivos de configuración.
\item Creación de los archivos contenedor \textbf{.cpio} (uno por cada arquitectura) que aglutina todas las bibliotecas, ejecutables y otros archivos de interés, así como los \textit{scripts} que llevarán a cabo la instalación del sistema operativo. 
\item Creación de los parámetros de la línea de comandos.
\item Copia de las utilidades de \textbf{MarcoPolo}.
\item Creación de un fichero \textbf{.zip}.
\end{enumerate}

\subsubsection*{buildupdate.sh}

El comportamiento es similar al del script \textbf{build.sh}, si bien este genera una imagen preparada para realizar actualizaciones del sistema en lugar de instalaciones.

\subsubsection*{deploy.sh}

Elimina el esquema de particiones de la tarjeta SD y crea uno nuevo con una única partición, donde descomprime el archivo \textbf{.zip} creado anteriormente.

Estos tres \textit{scripts} deben ser ejecutados en el orden en el que han sido expuestos, pues dependen de los archivos generados por el resto. En caso de que no se realice la llamada de uno sin su antecesor, este procederá a su ejecución antes de realizar la funcionalidad que le corresponde.

\subsubsection*{clean.sh}

Elimina todos los ficheros temporales y el resultado de las ejecuciones.

La mayoría de los archivos temporales que son descargados se conservan y son reutilizados en posteriores ejecuciones, optimizando el tiempo de instalación en varias máquinas.

\subsection{marco-bootstrap}

Ejecutable en C++ que permite llevar a cabo el descubrimiento del servidor donde se aloja el sistema operativo mediante \textbf{MarcoPolo}. Debido a la ausencia del \textit{daemon} de \textbf{Marco} en la imagen mínima copiada a la tarjeta SD, se utiliza una implementación básica del protocolo, denominada \textbf{marco-minimal}. Esta implementación expone la misma funcionalidad que el \textbf{binding} de Marco en C++, pero a más bajo nivel.

Se apoya en la biblioteca \textbf{RapidJSON} para la generación e interpretación de cadenas \textbf{JSON}, así como en la API de sockets del sistema. Con estas dos herramientas es capaz de implementar toda la funcionalidad básica que requiere para utilizar \textbf{MarcoPolo}. La gestión de la compilación se lleva a cabo con la herramienta \textbf{Cmake}. Se integra con el script rcS mediante las salidas estándar y de error.

\subsection{marco-bootstrap-backend}

Interfaz de gestión de los del sistema. Dicha herramienta está diseñada para el administrador de la red, y mediante la misma es posible programar operaciones de actualización en varios nodos, el reinicio de máquinas o la preparación de imágenes de instalación.

La herramienta se apoya en MarcoPolo para la detección de los diferentes nodos y está construida sobre los \textit{frameworks} \textbf{Tornado} y \textbf{Django}.

\subsubsection{Funcionamiento básico}

La interfaz se basa en la estructura de la herramienta MarcoDeployer: un conjunto de pestañas con funcionalidad relacionada.

\subsubsection{Vista principal}

En la vista principal el administrador puede visualizar las diferentes imágenes disponibles para su uso, los nodos sobre los que puede realizar operaciones, y un panel desde el que puede programar acciones sobre los nodos activos.

\begin{figure}[H]
\centering
\includegraphics[width=0.9\textwidth]{index}
\caption{Vista de la interfaz inicial.}
\label{backend:index}
\end{figure}

El administrador puede desde esta interfaz programar operaciones y enviarlas a un conjunto de nodos. Todas las operaciones pueden ser programadas para ser ejecutadas en un momento futuro.

\subsubsection{Operaciones programadas}

En ocasiones es necesario conocer las operaciones programadas para un nodo dado y contar con la posibilidad de cancelar las mismas. En la pestaña ``Programados'' es posible observar todos los eventos programados y los nodos sobre los que se aplicarán. Las operaciones se presentan en orden cronológico.
\subsubsection{Cancelación}

Todas las operaciones cuentan con un identificador único en el sistema. Utilizándolo es posible cancelar operaciones futuras.

\subsubsection{Persistencia}

Es necesario mantener una serie de datos de forma persistente que sirvan para conocer las operaciones a realizar y aquellas ya ejecutadas. La herramienta utiliza el gestor relacional de objetos (\textit{Object Relational Manager}) del \textit{framework} Django, que abstrae la interacción con una base de datos, además de gestionar la seguridad de las operaciones ejecutadas en la misma (en especial la protección contra el ataque conocido como ``Inyección SQL'', mitigado de forma automática por el gestor). La abstracción permite intercambiar la capa permanente de forma sencilla.

Debido a la modularidad del \textbf{framework}, es posible integrar el ORM en Tornado con poco esfuerzo, realizando únicamente una serie de ajustes previos en la configuración de la aplicación.

Como gestor de la base de datos se utiliza SQLite, un gestor que implementa un subconjunto del estándar SQL y que se caracteriza por su simplicidad y eficiencia. Es ideal para su uso en sistemas embebidos como las Raspberry Pi. Su simplicidad es tal que el gestor no se incluye como una pieza independiente sobre la que una serie de aplicaciones se conectan (tal es el caso de gestores de bases de datos como MySQL u Oracle Database), sino que se incluye dentro de la aplicación que hace uso de una base de datos.

En la base de datos existe una única tabla, que almacena el tiempo en el que se ha programado una operación, los nodos sobre la que se ha realizado, el tipo de operación y cualquier otro parámetro adicional.

\subsection{marco-bootstrap-backend-slave}

Los esclavos son los encargados de llevar a cabo las operaciones programadas, así como la cancelación de las mismas en caso de que sea necesario.

\subsubsection{Reinicio}

El reinicio del sistema se realiza mediante la orden \textbf{shutdown} proporcionada por el sistema operativo. Se ha dispuesto una espera de dos minutos previa al reinicio a fin de que los usuarios puedan guardar su trabajo de forma segura.

\subsubsection{Actualización}

Cada esclavo cuenta con una imagen del conjunto de herramientas necesarias para realizar el proceso de actualización, similares a las utilizadas en el proceso de instalación del sistema operativo. Dicha imagen es instalada en la primera partición (sobreescribiendo todos los archivos allí existentes de mismo nombre) y sus contenidos son leídos por la placa al realizarse el reinicio que se programa tras la extracción. Con el objetivo de garantizar la estabilidad del sistema, los ficheros son sobreescritos en el momento posterior al reinicio, y no en el momento en el que la operación es programada.

\subsubsection{Programación}

Los diferentes eventos se programan en el futuro utilizando el bucle de eventos de \textbf{Tornado}.
\subsection{Seguridad}

La autenticación del administrador se realiza mediante una combinación de usuario y contraseña (almacenada como \textit{hash} SHA de 256 bits). Todas las comunicaciones (exceptuando la descarga de una imagen de sistema operativo) se realizan mediante el protocolo HTTPS, y en el caso de la programación y cancelación de operaciones la identificación de cada una de las partes se realiza mediante certificados SSL tanto en cliente como en servidor.

\section{Instalación}

El conjunto de herramientas que realizan la instalación del sistema operativo están basadas en \textbf{Debian}, y por tanto utilizan el gestor de arranque \textbf{init}. Para realizar la instalación por tanto se delegará la ejecución de los \textit{scripts} que realicen las funciones a dicho gestor. Por ello, se define el script \texttt{/etc/rcS}, que es ejecutado en cualquier nivel de ejecución (\textit{runlevel})\footnote{\href{http://apt-browse.org/browse/ubuntu/trusty/main/all/sysv-rc/2.88dsf-41ubuntu6/file/etc/rcS.d/README}{apt-browse.org/browse/ubuntu/trusty/main/all/sysv-rc/2.88dsf-41ubuntu6/file/etc/rcS.d/README}}. 

El script lleva a cabo la siguiente secuencia de tareas:

\begin{enumerate}
	\item Define una serie de variables de utilidad.
	\item Crea de directorios necesarios dentro de la tarjeta SD.
	\item ``Instala'' todas las utilidades de \textbf{BusyBox} (permite el acceso a las mismas) y define las rutas de búsqueda de ficheros mediante la variable \texttt{\$PATH}.
	\item Monta de los pseudosistemas de ficheros \texttt{/proc} y \texttt{/sys}.
	\item Establece mecanismos de redirección de una copia de las salidas estándar y de error (\textbf{stdout} y \textbf{stderr}) a un fichero de log para su posterior análisis.
	\item Determina el tipo de \textit{hardware} sobre el que se está ejecutando el \textit{script}, a fin de instalar versión del sistema operativo apropiada.
	\item Copia los ficheros de arranque contenidos en la tarjeta SD.
	\item Carga un \textit{script} con parámetros adicionales definidos por el usuario.
	\item Configura de la interfaz de red \textbf{eth0} mediante \textbf{DHCP}.
	\item Determina de la hora mediante \textbf{NTP}.
	\item Carga módulos del \textit{kernel} si son necesarios.
	\item Particiona la tarjeta SD según el siguiente esquema:
	\begin{itemize}
		\item Partición 1: 128 megabytes, formato FAT32.
		\item Partición 2: Resto de la tarjeta SD, formato ext4.
	\end{itemize}
	\item Busca de servidores que alojen el sistema operativo mediante \textbf{MarcoPolo}.
	\item Descarga el sistema operativo como archivo \texttt{.tar.gz}.
	\item Descomprime el sistema operativo en la partición número 2 (donde la mayoría de ficheros se almacenan. La estructura del fichero \texttt{.tar.gz} define el lugar donde cada fichero se debe alojar, y los permisos de acceso a los ficheros son conservados en el proceso de extracción, por lo que con la extracción se consigue además la estructuración del sistema y la gestión de permisos).
	\item Mueve los ficheros de arranque a la partición 1 (\textit{kernel}, parámetros de arranque, etcétera).
	\item Ejecuta los \textit{scripts} de post-instalación.
	\item Almacena el fichero de log.
	\item Limpia archivos temporales.
	\item Desmonta los sistemas de ficheros y procede al reinicio del sistema.
\end{enumerate}

Una vez ejecutado el script el nodo está preparado para integrarse en el sistema. El tiempo total de instalación es de unos 5 minutos, según el fichero de log, dependiendo del estado de la red. En pruebas realizadas, el tiempo de descarga supone aproximadamente la mitad del tiempo total de instalación, si bien mejora significativamente cuando se cuenta con un servidor local que aloje el sistema operativo.

\section{Actualización}

El administrador puede programar actualizaciones utilizando la herramienta de backend y enviar notificaciones a los diferentes nodos del sistema. Estos realizarán la actualización en el siguiente reinicio del sistema, o si lo desea el administrador, de forma programada o inmediata.

El administrador únicamente debe subir a la interfaz web una imagen del sistema operativo a replicar, que incluirá todos los paquetes necesarios. Los nodos reemplazarán su sistema operativo por este mediante un proceso similar al de instalación.

Para realizar la creación de la imagen existe la herramienta \texttt{createimage.sh}, que requiere como parámetro un descriptor de dispositivo (una tarjeta SD o un archivo .img) que contenga los archivos a copiar.

Los nodos cuentan con un servicio (vinculado a MarcoPolo) que escucha peticiones (verificadas por un certificado SSL) de actualización (o cualquier otra funcionalidad que pueda ser añadida, apostando por la versatilidad en el desarrollo de la plataforma). Al recibir una petición válida (el certificado enviado pertenece a un servidor de despliegue), comienza el siguiente proceso:

\begin{itemize}
	\item Comprueban los parámetros de la petición, y actúan en consecuencia.
	\item En el caso de que se solicite una actualización, se leen los parámetros de la petición, que incluyen el momento de reinicio y el nombre del fichero a solicitar al servidor.
	\item Copia a la partición 1 (arranque) los ficheros kernel.img, paquetes Debian necesarios, Busybox y el script /etc/init/rcS, que en caso de ser necesario, deberá ser modificado para atender a las características propias de cada consulta.
	\item Programa un reinicio del sistema en el momento definido por el administrador (inmediatamente, en un momento determinado o en el momento en el que se produzca un reinicio originado por otra causa).
	\item Al reiniciarse se realiza una secuencia de pasos similar a la del proceso de instalación.

\end{itemize}

A fin de no eliminar la información almacenada por los usuarios, o cualquier otro tipo de datos que deban sobrevivir a este tipo de actualizaciones, se definirán particiones que no serán alteradas.

\section{Tiempo}

El tiempo de interacción que el administrador debe emplear por cada máquina es de aproximadamente un minuto, el tiempo necesario para conectar una tarjeta SD, ejecutar el script de carga de los paquetes de instalación, y la desconexión de la misma.

El tiempo de instalación es variable, dependiente principalmente de la velocidad de descarga del sistema operativo y el tiempo de descompresión y copia de este, si bien suele ser inferior a 10 minutos (y este tiempo es paralelo para todas las máquinas, suponiendo únicamente una mayor carga para el servidor).

\section{Evaluación}

Se han realizado las siguientes evaluaciones de uso:

\begin{itemize}
	\item Entrevista con el Administrador de la instalación el día 13 de mayo, donde se le presenta la solución propuesta y los resultados obtenidos. Según su criterio considera que el enfoque es el adecuado.

\end{itemize}
\nocite{rpiconfig}

 \label{Bibliography}
 \lhead{\emph{Bibliografía}}  % Change the left side page header to "Bibliography"
 \bibliographystyle{ieeetr}  % Use the "unsrtnat" BibTeX style for formatting the Bibliography
 \bibliography{marcobootstrap}  % The references (distcc) information are stored in the file named "distcc.bib"

\end{document}
