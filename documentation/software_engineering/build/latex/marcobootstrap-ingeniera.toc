\select@language {spanish}
\contentsline {chapter}{\numberline {1}Fase de an\IeC {\'a}lisis}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Dominio del problema}{1}{section.1.1}
\contentsline {section}{\numberline {1.2}Objetivos}{1}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}OBJ1 Programaci\IeC {\'o}n de operaciones}{1}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}OBJ2 Instalaci\IeC {\'o}n desatendida}{2}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}OBJ3 Actualizaci\IeC {\'o}n}{2}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}OBJ4 \emph {Zeroconf}}{2}{subsection.1.2.4}
\contentsline {section}{\numberline {1.3}Actores}{3}{section.1.3}
\contentsline {subsection}{\numberline {1.3.1}\textbf {ACT1} Administrador}{3}{subsection.1.3.1}
\contentsline {subsection}{\numberline {1.3.2}\textbf {ACT2} Servidor}{3}{subsection.1.3.2}
\contentsline {subsection}{\numberline {1.3.3}\textbf {ACT3} Nodo}{3}{subsection.1.3.3}
\contentsline {section}{\numberline {1.4}Requisitos de informaci\IeC {\'o}n}{3}{section.1.4}
\contentsline {subsection}{\numberline {1.4.1}\textbf {IRQ1}: Gesti\IeC {\'o}n de usuarios}{3}{subsection.1.4.1}
\contentsline {subsection}{\numberline {1.4.2}\textbf {IRQ2}: Registro de operaciones}{4}{subsection.1.4.2}
\contentsline {subsection}{\numberline {1.4.3}\textbf {IRQ3}: Registro de operaciones local}{4}{subsection.1.4.3}
\contentsline {section}{\numberline {1.5}Requisitos funcionales}{5}{section.1.5}
\contentsline {subsection}{\numberline {1.5.1}\textbf {RF1}: Programar operaci\IeC {\'o}n}{5}{subsection.1.5.1}
\contentsline {subsection}{\numberline {1.5.2}\textbf {RF2} Ordenar cancelaci\IeC {\'o}n}{6}{subsection.1.5.2}
\contentsline {subsection}{\numberline {1.5.3}\textbf {RF3} Consulta al registro de operaciones}{6}{subsection.1.5.3}
\contentsline {subsection}{\numberline {1.5.4}\textbf {RF4} Consulta de operaciones programadas}{7}{subsection.1.5.4}
\contentsline {subsection}{\numberline {1.5.5}\textbf {RF5} Consulta de hist\IeC {\'o}rico}{8}{subsection.1.5.5}
\contentsline {subsection}{\numberline {1.5.6}\textbf {RF6} Descarga del sistema operativo}{8}{subsection.1.5.6}
\contentsline {subsection}{\numberline {1.5.7}\textbf {RF7} Instalaci\IeC {\'o}n}{9}{subsection.1.5.7}
\contentsline {subsection}{\numberline {1.5.8}\textbf {RF8} Actualizaci\IeC {\'o}n}{10}{subsection.1.5.8}
\contentsline {subsection}{\numberline {1.5.9}\textbf {RF9} Gestionar error en la instalaci\IeC {\'o}n}{10}{subsection.1.5.9}
\contentsline {subsection}{\numberline {1.5.10}\textbf {RF10} Gestionar error en backend}{11}{subsection.1.5.10}
\contentsline {subsection}{\numberline {1.5.11}\textbf {RF11} Autenticaci\IeC {\'o}n}{12}{subsection.1.5.11}
\contentsline {subsection}{\numberline {1.5.12}\textbf {RF12} Creaci\IeC {\'o}n de imagen del sistema operativo}{12}{subsection.1.5.12}
\contentsline {subsection}{\numberline {1.5.13}\textbf {RF13} Creaci\IeC {\'o}n de imagen de instalaci\IeC {\'o}n o actualizaci\IeC {\'o}n}{13}{subsection.1.5.13}
\contentsline {subsection}{\numberline {1.5.14}\textbf {RF14} Realizar operaci\IeC {\'o}n}{14}{subsection.1.5.14}
\contentsline {subsection}{\numberline {1.5.15}\textbf {RF15} Cancelar operaci\IeC {\'o}n}{14}{subsection.1.5.15}
\contentsline {subsection}{\numberline {1.5.16}Vista de casos de uso}{15}{subsection.1.5.16}
\contentsline {subsubsection}{Vista de paquetes de casos de uso}{15}{subsubsection*.3}
\contentsline {section}{\numberline {1.6}Vista de paquetes de casos de uso}{15}{section.1.6}
\contentsline {section}{\numberline {1.7}Requisitos no funcionales}{15}{section.1.7}
\contentsline {subsection}{\numberline {1.7.1}\textbf {NFR1} Tornado}{15}{subsection.1.7.1}
\contentsline {subsection}{\numberline {1.7.2}\textbf {NFR2} Abstracci\IeC {\'o}n de la base de datos}{18}{subsection.1.7.2}
\contentsline {subsection}{\numberline {1.7.3}\textbf {NFR3} HTTPS con doble validaci\IeC {\'o}n}{18}{subsection.1.7.3}
\contentsline {subsection}{\numberline {1.7.4}\textbf {NFR4} Compatibilidad con systemd e initd}{18}{subsection.1.7.4}
\contentsline {subsection}{\numberline {1.7.5}\textbf {NFR5} Instalaci\IeC {\'o}n a trav\IeC {\'e}s de \texttt {setuptools}}{19}{subsection.1.7.5}
\contentsline {subsection}{\numberline {1.7.6}\textbf {NFR6} Registro}{19}{subsection.1.7.6}
\contentsline {subsection}{\numberline {1.7.7}\textbf {NRF7} Validaci\IeC {\'o}n de los datos de usuario}{19}{subsection.1.7.7}
\contentsline {section}{\numberline {1.8}Vista est\IeC {\'a}tica del sistema}{20}{section.1.8}
\contentsline {section}{\numberline {1.9}Vista de actividad}{20}{section.1.9}
\contentsline {subsection}{\numberline {1.9.1}Programaci\IeC {\'o}n y cancelaci\IeC {\'o}n de un evento}{20}{subsection.1.9.1}
\contentsline {subsection}{\numberline {1.9.2}Proceso de instalaci\IeC {\'o}n del sistema operativo}{21}{subsection.1.9.2}
\contentsline {section}{\numberline {1.10}Vista de m\IeC {\'a}quina de estados}{22}{section.1.10}
\contentsline {section}{\numberline {1.11}Vista de interacci\IeC {\'o}n}{22}{section.1.11}
\contentsline {subsection}{\numberline {1.11.1}Consulta a la base de datos}{22}{subsection.1.11.1}
\contentsline {section}{\numberline {1.12}Ingenier\IeC {\'\i }a web}{23}{section.1.12}
\contentsline {subsection}{\numberline {1.12.1}Mapa navegacional}{23}{subsection.1.12.1}
\contentsline {chapter}{\numberline {2}Fase de dise\IeC {\~n}o}{25}{chapter.2}
\contentsline {section}{\numberline {2.1}Vista est\IeC {\'a}tica del sistema}{26}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Paquetes backend y models}{26}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Paquete slave}{27}{subsection.2.1.2}
\contentsline {section}{\numberline {2.2}\IeC {\'A}mbito}{27}{section.2.2}
\contentsline {section}{\numberline {2.3}Dise\IeC {\~n}o de datos}{27}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}Objetos de datos}{27}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Estructuras de archivo}{27}{subsection.2.3.2}
\contentsline {section}{\numberline {2.4}Vista de interacci\IeC {\'o}n}{28}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Consulta a la base de datos}{28}{subsection.2.4.1}
\contentsline {section}{\numberline {2.5}Interfaz gr\IeC {\'a}fica de usuario}{28}{section.2.5}
\contentsline {section}{\numberline {2.6}Despliegue}{29}{section.2.6}
\contentsline {section}{\numberline {2.7}Dise\IeC {\~n}o procedimental}{30}{section.2.7}
\contentsline {subsection}{\numberline {2.7.1}Detecci\IeC {\'o}n del sistema operativo y descarga}{30}{subsection.2.7.1}
\contentsline {subsection}{\numberline {2.7.2}Programaci\IeC {\'o}n de una operaci\IeC {\'o}n de actualizaci\IeC {\'o}n}{31}{subsection.2.7.2}
\contentsline {subsection}{\numberline {2.7.3}Creaci\IeC {\'o}n de una operaci\IeC {\'o}n}{32}{subsection.2.7.3}
\contentsline {section}{\numberline {2.8}Pruebas}{33}{section.2.8}
\contentsline {section}{\numberline {2.9}Patrones de dise\IeC {\~n}o destacados}{33}{section.2.9}
\contentsline {chapter}{\numberline {3}Gesti\IeC {\'o}n del proyecto}{35}{chapter.3}
\contentsline {chapter}{\numberline {4}Indices and tables}{37}{chapter.4}
