from tornado.testing import AsyncTestCase, gen_test, AsyncHTTPClient


class IndexTestCase(AsyncTestCase):
	@gen_test
	def test_http_index(self):
		client = AsyncHTTPClient(self.io_loop)
		response = yield client.fetch("http://localhost:8080/")

		self.assertIn("Marco Bootstrap", response.body)
		self.assertIn("Files", response.body)

	# @gen_test
	# def test_http_tar(self):
	# 	client = AsyncHTTPClient(self.io_loop)
	# 	response = yield client.fetch("http://localhost:8080/tar/ArchLinuxARM-rpi-2-latest.tar.gz")
	# 	self.assertEqual(200, response.code)