#!/usr/bin/env python

import requests
from requests.adapters import HTTPAdapter
import os
import mimetypes,time
import conf
class NotCheckingHostnameHTTPAdapter(HTTPAdapter):
	def cert_verify(self, conn, *args, **kwargs):
		super(NotCheckingHostnameHTTPAdapter, self).cert_verify(conn, *args, **kwargs)
		conn.assert_hostname = False

websession = requests.session()
websession.mount('https://', NotCheckingHostnameHTTPAdapter())

from requests_futures.sessions import FuturesSession
futures_session = FuturesSession()
futures_session.mount('https://', NotCheckingHostnameHTTPAdapter())

s = requests.session()
s.mount('https://', NotCheckingHostnameHTTPAdapter())

filename = 'backend.py.zip'
__UPLOADS__ = os.path.dirname(os.path.realpath(__file__))

def get_content_type(filename):
	return mimetypes.guess_type(filename)[0] or 'application/octet-stream'
files = {'bootcode': (filename, open(os.path.join(__UPLOADS__, filename), 'rb'), get_content_type(filename))}
commands = {'image':'ArchLinuxARM-rpi-2-latest.tar.gz',
	'time':time.time()+1.0}
url = "https://localhost:1360/update"
s.post(url, files=files, data=commands, verify=conf.RECEIVERCERT, cert=(conf.APPCERT, conf.APPKEY))

url = "https://localhost:1360/reboot"

time_to_delete = time.time()+2.0;
commands["time"] = time_to_delete
s.post(url, files=files, data=commands, verify=conf.RECEIVERCERT, cert=(conf.APPCERT, conf.APPKEY))
url_cancel = "https://localhost:1360/cancel"
commands = {'type':'reboot','time':time_to_delete}

s.post(url_cancel, data=commands, verify=conf.RECEIVERCERT, cert=(conf.APPCERT, conf.APPKEY))
