/**
@file marcominimal.h
@brief A minimal implementation of the Marco role in the MarcoPolo protocol
*/
#ifndef MARCO_MINIMAL
#define MARCO_MINIMAL

#include <iostream>
#include <vector>

using namespace std;

/**
	@param service The service name
	@param max_nodes The maximum number of nodes to return
	@param exclude The list of nodes not to return
	@param timeout The waiting time in milliseconds
*/
vector<string> request_for(const wchar_t* service, int max_nodes=-1, char* exclude[]=NULL, int timeout=-1);

#endif