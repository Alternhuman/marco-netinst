#include <iostream>
#include <exception>
#include <vector>

#include <sys/socket.h>
#include <stdio.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <strings.h>

#include <sstream>
#include <string>

#include <string>
#include <iterator>
#include <stdlib.h>
#include <iconv.h>


#include "marcoexception.h"
#include <rapidjson/document.h>
#include <rapidjson/writer.h>
#include <rapidjson/stringbuffer.h>

#include "JSON.h"
#include "JSONValue.h"
#include "utf8.h"

#define UTF8_SEQUENCE_MAXLEN 8
#define RESPONSE_LEN 250
using namespace std;

vector<string> request_for(const wchar_t* service, int max_nodes=-1, char* exclude[]=NULL, int timeout=-1){
    vector<string> return_hosts;
    int sd = socket(AF_INET, SOCK_DGRAM, 0);
    struct sockaddr_in bind_addr;

    if (sd < 0){
        perror("Internal error");
        return return_hosts;
    }
    
    struct sockaddr_in groupSock;
    memset((char *) &groupSock, 0, sizeof(groupSock));
    groupSock.sin_family = AF_INET;
    groupSock.sin_addr.s_addr = inet_addr("224.0.0.112");
    groupSock.sin_port = htons(1337);

    struct in_addr localInterface;
    localInterface.s_addr = INADDR_ANY;

    if(setsockopt(sd, IPPROTO_IP, IP_MULTICAST_IF, (char *)&localInterface, sizeof(localInterface)) < 0)
    {
        perror("Setting local interface error");
        exit(1);
    }

    struct timeval timeout_val;  
    if (timeout > 0){
        timeout_val.tv_sec = timeout/1000;
        timeout_val.tv_usec = 0;
    }else{
        timeout_val.tv_sec = 5;
        timeout_val.tv_usec = 0;
    }
    

    if (setsockopt (sd, SOL_SOCKET, SO_RCVTIMEO, (char *)&timeout_val, sizeof(timeout_val)) < 0){
        perror("setsockopt failed\n");
        return return_hosts;
    }



    JSONObject j;
    j[L"Command"] = new JSONValue(L"Request-For");
    j[L"Params"] = new JSONValue(service);

    JSONValue *value = new JSONValue(j);    
    const wchar_t *wcs = value->Stringify().c_str();

    signed char utf8[(wcslen(wcs) + 1 /* L'\0' */) * UTF8_SEQUENCE_MAXLEN];
    char *iconv_in = (char *) wcs;
    char *iconv_out = (char *) &utf8[0];
    size_t iconv_in_bytes = (wcslen(wcs) + 1 /* L'\0' */) * sizeof(wchar_t);
    size_t iconv_out_bytes = sizeof(utf8);
    size_t ret;
    
    size_t b = wchar_to_utf8(wcs, iconv_in_bytes, iconv_out, iconv_out_bytes, 0);
    if(b < 1){
        perror("Internal error during UTF-8 conversion in marcousers");
        return return_hosts;
    }

    if(sendto(sd, iconv_out, strlen(iconv_out), 0, (struct sockaddr*)&groupSock, sizeof(groupSock)) < 0)
    {
        perror("Sending datagram message error");
    }

    char recv_response[RESPONSE_LEN];

    struct sockaddr_in src_addr;
    socklen_t addrlen = sizeof(src_addr);
    size_t response = recvfrom(sd, recv_response, RESPONSE_LEN,0, (sockaddr*)&src_addr, &addrlen);
    
    if (response == -1){
        perror("Timeout");
        return return_hosts;
    }

    wchar_t to_arr[response];
    char to_arr_aux[response];
    strncpy(to_arr_aux, recv_response, response);
    
    size_t c = utf8_to_wchar(to_arr_aux, response, to_arr, response, 0);
    
    wstring ws(to_arr);
    
    string str(ws.begin(), ws.end());
    str = str.substr(0, c);

    rapidjson::Document document;
    
    if(document.Parse<0>(str.c_str()).HasParseError()){
        throw marcoexception("Internal error on parsing");
    }
    assert(document.IsObject());
    
    return_hosts.push_back(inet_ntoa(src_addr.sin_addr));
    
    return return_hosts;

}