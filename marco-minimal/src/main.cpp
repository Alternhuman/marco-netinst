#include "marcominimal.h"
#include <stdio.h>
#include <vector>
#include <string>

int main(int argc, char * argv[]){
    std::vector<string> nodes = request_for(L"marcobootstrap");
    if(nodes.size() > 0){
        fprintf(stdout, "%s", nodes[0].c_str());
        return 0;
    }else{
        fprintf(stderr, "No nodes available");
        return 1;
    }
}