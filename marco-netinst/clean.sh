#!/bin/sh

## @brief Removes all temporary files
## @file clean.sh

rm -rf bootfs rootfs firmware packages tmp installer*.cpio *.zip *.xz *.bz2 *.img
