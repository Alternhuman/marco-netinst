#/!usr/bin/env python
import sys

from passlib.hash import sha256_crypt

# generate new salt, and hash a password
hash = sha256_crypt.encrypt(sys.argv[1])

print(hash)