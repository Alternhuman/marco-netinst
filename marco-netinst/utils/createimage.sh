#!/bin/bash

## @file createimage.sh
## @brief Extracts the contents of an SD card and creates a tar.gz replicable file

if [[ $# -lt 2 ]];then
        echo "Usage: $0 <Device> <filename>"
        exit 1
fi

DEVICE=$1
FILE=$2

if [ ! -e $DEVICE ]; then
        echo "Device does not exist"
        exit 1
fi
if [[ $DEVICE =~ "/dev/mmcblk" ]];then
prefix="p"
fi
if [ "$(id -u)" != "0" ];then
        echo "You need to be root to execute the script. Please retry"
        exit 1
fi

if [ -d /tmp/createimgtmp ]; then
if mountpoint -q -- "/tmp/createimgtmp";then
	umount /tmp/createimgtmp
fi
rm -rf /tmp/createimgtmp
fi

if [ -d /tmp/createimgmnt ]; then
if mountpoint -q -- "/tmp/createimgmnt";then
	umount /tmp/createimgmnt
fi
rm -rf /tmp/createimgmnt
fi

mkdir -p /tmp/createimgtmp/boot
mkdir /tmp/createimgmnt

echo "Unmounting the device"

umount ${DEVICE}${prefix}?

echo "Partition 1"

mount ${DEVICE}${prefix}1 /tmp/createimgmnt

cp -r /tmp/createimgmnt/* /tmp/createimgtmp/boot

umount /tmp/createimgmnt

echo "Partition 2"

mount ${DEVICE}${prefix}2 /tmp/createimgmnt

#cp -r /tmp/createimgmnt/* /tmp/createimgtmp/
rsync -a --progress /tmp/createimgmnt/* /tmp/createimgtmp/ --exclude /tmp/createimgtmp/boot
#rsync -ah --progress source-file destination
umount /tmp/createimgmnt

echo "Compressing..."

tar -czf $2 -C /tmp/createimgtmp/ .

rm -r /tmp/createimgtmp
rm -r /tmp/createimgmnt

echo "All done!"
