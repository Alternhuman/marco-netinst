# marco-netinst

The minimal Arch Linux netinstaller for Raspberry Pi Models 1B, 1B+, 2B

This project, based on [raspbian-ua-netinst][1], allows users of Arch Linux to install the base system (using Arch Linux ARM official tarballs or a custom-defined tarball, using MarcoPolo). As such, it is a great tool for sysadmins, due to the fact that the installation can be left completely unattended, and only ~15 MB have to be manually copied to an SD card. 

The default settings uses DHCP for network configuration, wipes and formats the SD card, gets all the necessary files and copies them. Further configuration can be performed using standard bash scripts.


<!--There are different kinds of "presets" that define the default packages that are going to be installed. Currently, the default one is called _server_ which installs only the essential base system packages including _NTP_ and _OpenSSH_ to provide a sane minimal base system that you can immediately after install ssh in and continue installing your software.

Other presets include _minimal_ which has even less packages (no logging, no text editor, no cron) and _base_ which doesn't even have networking. You can customize the installed packages by adding a small configuration file to your SD card before booting up.-->

## Features
 - completely unattended, you only need working Internet connection through the Ethernet port
 - DHCP and static ip configuration (DHCP is the default)
 - always installs the latest version of Arch Linux, allowing the usage of a custom version
 - configurable default settings
 - extra configuration over HTTP possible - gives unlimited flexibility
 - installation takes about **15 minutes** with fast Internet from power on to sshd running
 - /tmp is mounted as tmpfs to improve speed
 - no clutter included, you only get the bare essential packages
 - option to install root to USB drive

## Requirements
 - a Raspberry Pi Model 1B, 1B+ or 2B
 - SD card of at least 640MB
 - working Ethernet with Internet connectivity

## Obtaining installer files on Windows and Mac
Installer archive is around **17MB** and contains all firmware files and the installer.

##Deploying on Linux

The script *deploy.sh* should take care of everything in Linux environments when the dependency packages are properly installed.


## Installing
In normal circumstances, you can just power on your Pi and cross your fingers.

If you don't have a display attached you can monitor the Ethernet card leds to guess activity. When it finally reboots after installing everything you will see them going out and on a few times when Raspbian configures it on boot.

If you do have a display, you can follow the progress and catch any possible errors in the default configuration or your own modifications.  
If you have a serial cable, then remove 'console=tty1' at then end of the `cmdline.txt` file.

**Note:** During the installation you'll see various warning messages, like "Warning: cannot read table of mounted file systems" and "dpkg: warning: ignoring pre-dependency problem!". Those are expected and harmless.

### Logging
The output of the installation process is now also logged to file.  
When the installation completes successfully, the logfile is moved to /var/log/archlinux-netinst.log on the installed system.  
When an error occurs during install, the logfile is moved to the sd card, which gets normally mounted on /boot/ and will be named archlinux-netinst-\<datetimestamp\>.log

## First boot
The system is almost completely unconfigured on first boot. Here are some tasks you most definitely want to do on first boot.

The default **root** password is **root**.

<!-- Set new root password: `passwd`  (can also be set during installation using **rootpw** in [installer-config.txt](#installer-customization))  
> Configure your default locale: `dpkg-reconfigure locales`  
> Configure your timezone: `dpkg-reconfigure tzdata`  

The latest kernel and firmware packages are now automatically installed during the unattended installation process.
When you need a kernel module that isn't loaded by default, you will still have to configure that manually.
When a new kernel becomes available in the archives and is installed, the system will update config.txt, so it boots up the new kernel at the next reboot.

## Reinstalling or replacing an existing system
If you want to reinstall with the same settings you did your first install you can just move the original _config.txt_ back and reboot. Make sure you still have _kernel_install.img_ and _installer.cpio.gz_ in your _/boot_ partition. If you are replacing your existing system which was not installed using this method, make sure you copy those two files in and the installer _config.txt_ from the original image.

    mv /boot/config-reinstall.txt /boot/config.txt
    reboot

**Remember to backup all your data and original config.txt before doing this!**
-->

## Installer customization
While defaults should work for most power users, some might want to customize default configuration or the package set even further. The installer provides support for this by reading a configuration file `installer-config.txt` from the first vfat partition. The configuration file is read in as a shell script so you can abuse that fact if you so want to. 
See `scripts/etc/init.d/rcS` for more details of what kind of environment your script will be run in (currently 'busybox sh'). 

If an `installer-config.txt` file exists in the same directory as this `README.md`, it will be added to the installer image automatically.

The format of the file and the current defaults:

    preset=server
    packages= # comma separated list of extra packages
    mirror=http://mirrordirector.raspbian.org/raspbian/
    release=wheezy
    hostname=pi
    domainname=
    rootpw=raspbian
    cdebootstrap_cmdline=
    bootsize=+128M # /boot partition size in megabytes, provide it in the form '+<number>M' (without quotes)
    rootsize=     # / partition size in megabytes, provide it in the form '+<number>M' (without quotes), leave empty to use all free space
    timeserver=time.nist.gov
    ip_addr=dhcp
    ip_netmask=0.0.0.0
    ip_broadcast=0.0.0.0
    ip_gateway=0.0.0.0
    ip_nameservers=
    online_config= # URL to extra config that will be executed after installer-config.txt
    usbroot= # set to 1 to install to first USB disk
    cmdline="dwc_otg.lpm_enable=0 console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty1 elevator=deadline"
    rootfstype=ext4
    rootfs_mkfs_options=
    rootfs_install_mount_options='noatime,data=writeback,nobarrier,noinit_itable'
    rootfs_mount_options='errors=remount-ro,noatime'

All of the configuration options should be clear. You can override any of these in your _installer-config.txt_ by placing your own `installer-config.txt` in the main directory.  
The time server is only used during installation and is for _rdate_ which doesn't support the NTP protocol.  
**Note:** You only need to provide the options which you want to **override** in your _installer-config.txt_ file.  
All non-provided options will use the defaults as mentioned above.

There's also support for a `post-install.txt` script which is executed just before unmounting the filesystems. You can use it to tweak and finalize your automatic installation. Just like above, if `post-install.txt` exists in the same directory as this `README.md`, it will be added to the installer image automatically.

## Disclaimer
I take no responsibility for ANY data loss. You will be reflashing your SD card anyway so it should be very clear to you what you are doing and will lose all your data on the card. Same goes for reinstallation.

See LICENSE for license information.

  [1]: https://github.com/debian-pi/raspbian-ua-netinst "raspbian-ua-netinst"
  [2]: http://www.raspbian.org/ "Raspbian"
