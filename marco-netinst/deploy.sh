#!/bin/bash

## @brief Deploys the installer in the specified device
## @file deploy.sh

ALLYES=0
DEVICE=""
if [[ $# -lt 1 ]];then
        echo "Usage: [-y] $0 <Device>"
        exit 1
fi

for arg in "$@"
do
	case $arg in
	"-y")
	ALLYES=1
	;;
	*)
	DEVICE=$arg
	;;
	esac
done

if [ ! -e $DEVICE ]; then
        echo "Device does not exist"
        exit 1
fi
if [[ $DEVICE =~ "/dev/mmcblk" ]];then
prefix="p"
fi

if [ "$(id -u)" != "0" ];then
        echo "You need to be root to execute the script. Please retry"
        exit 1
fi
if [[ $ALLYES -eq 0 ]];then
	echo "I will erase all contents on ${DEVICE} and then install the selected OS. Are you sure that you want to proceed? (Y/n)"

read confirmation

else
	confirmation="Y"
fi

if [[ $confirmation != "Y" ]];then
        exit 1
fi

echo "Starting. Unmounting devices"
umount ${DEVICE}${prefix}?

echo "Partitioning"
fdisk ${DEVICE} <<EOF
o
n
p
1


t
c
w
EOF

echo "Creating filesystems and mounting them"
mkfs.vfat ${DEVICE}${prefix}1

if [ -d /tmp/root ]; then
if mountpoint -q -- "/tmp/root";then
	umount /tmp/root
fi
rm -rf /tmp/root
fi

mkdir /tmp/root

umount ${DEVICE}${prefix}1
mount ${DEVICE}${prefix}1 /tmp/root

echo "Copying files"

bash build.sh
ZIPFILE=raspbian-ua-netinst-`date +%Y%m%d`-git`git rev-parse --short @{0}`.zip
unzip $ZIPFILE -d /tmp/root
sync

echo "All done"
umount ${DEVICE}${prefix}?
exit 0
